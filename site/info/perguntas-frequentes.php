<?php include_once(__DIR__.'/../include/header.php'); ?>

<div class="ui breadcrumb">
	<a href="../index/" class="section">Início</a>
	<i class="right angle icon divider"></i>
	Perguntas Frequentes
</div>

<div class="ui segments">
	<div class="ui segment">
		<h1>Perguntas Frequentes</h1>
	</div>
 	<div class="ui secondary segment">
		<div class="ui accordion">
			<div class="title">
				<i class="dropdown icon"></i>
				1. O que é a Lei de Acesso à Informação?
			</div>
			<div class="content">
				<p>A Lei nº 12.527/2011, conhecida como Lei de Acesso à Informação - LAI, regulamenta o direito, previsto na Constituição, de qualquer pessoa solicitar e receber dos órgãos e entidades públicos, de todos os entes e Poderes, informações públicas por eles produzidas ou custodiadas.</p>
			</div>

			<div class="title">
				<i class="dropdown icon"></i>
				2. Quando a Lei de Acesso à Informação entrou em vigor?
			</div>
			<div class="content">
				<p>A Lei de Acesso à Informação foi publicada em 18 de novembro de 2011, mas só entrou em vigor 180 (cento e oitenta) dias após essa data, ou seja, em 16 de maio de 2012</p>
			</div>

			<div class="title">
				<i class="dropdown icon"></i>
				3. Quem é obrigado a cumprir a Lei de Acesso à Informação?
			</div>
			<div class="content">
				<p>A Lei de Acesso à Informação brasileira se aplica a toda a administração pública, ou seja, a todos os órgãos e entidades dos poderes Executivo, Legislativo e Judiciário, da União, dos Estados, do Distrito Federal e dos Municípios, bem como a todos os Tribunais de Contas e ao Ministério Público (Art. 1°). Além da administração pública, a Lei abrange as entidades privadas sem fins lucrativos que recebem recursos públicos (Art. 2°). </p>
				<p>Municípios até 10.000 habitantes estão dispensados dessa obrigatoriedade, devendo cumprir apenas com o determinado pela Lei de Responsabilidade Fiscal("Divulgação em tempo real, de informações relativas à execução orçamentária e financeira, nos critérios e prazos previstos no Artº 73-B da Lei Complementar nº101, de 04 de maio de 2000").</p>
			</div>

			<div class="title">
				<i class="dropdown icon"></i>
				4. Para cada estado, município e o Distrito Federal haverá legislação própria regulamentando o direito de acesso à informação?
			</div>
			<div class="content">
				<p>Sim, a Lei de Acesso à Informação contém dispositivos gerais, aplicáveis indistintamente a todos os que estão sujeitos a ela, e alguns dispositivos que são aplicáveis somente ao Poder Executivo Federal. </p>
				<p>O Art. 45 da Lei define que cabe aos Estados, ao Distrito Federal e aos Municípios definir suas regras específicas em legislação própria, obedecidas as normas gerais estabelecidas na Lei de Acesso. </p>
				<p>É importante ressaltar que os dispositivos gerais têm aplicação imediata. Portanto, a falta de regulamentação específica prejudica, mas não impede o cumprimento da Lei.</p>
			</div>

			<div class="title">
				<i class="dropdown icon"></i>
				5. A que tipo de informação os cidadãos podem ter acesso pela Lei de Acesso?
			</div>
			<div class="content">
				<p>Com a Lei de Acesso, a publicidade passou a ser a regra e o sigilo a exceção. Dessa forma, as pessoas podem ter acesso a qualquer informação pública produzida ou custodiada pelos órgãos e entidades da Administração Pública. A Lei de Acesso, entretanto, prevê algumas exceções ao acesso às informações, notadamente àquelas cuja divulgação indiscriminada possa trazer riscos à sociedade ou ao Estado.</p>
			</div>

			<div class="title">
				<i class="dropdown icon"></i>
				6. É preciso justificar o pedido de acesso à informação?
			</div>
			<div class="content">
				<p>Não. De acordo com o art. 10, § 3° da Lei de Acesso, é proibido exigir que o solicitante informe os motivos de sua solicitação. Entretanto, o órgão/entidade pode dialogar com o cidadão para entender melhor a demanda, de modo a fornecer a informação mais adequada a sua solicitação.</p>
			</div>

			<div class="title">
				<i class="dropdown icon"></i>
				7. O acesso à informação é gratuito?
			</div>
			<div class="content">
				<p>Conforme dispõe o Art. 12 da Lei de Acesso à Informação, o serviço de busca e fornecimento da informação é gratuito. Entretanto, podem ser cobrados os custos dos serviços e dos materiais utilizados na reprodução e envio de documentos. Neste caso, o órgão ou entidade deverá disponibilizar ao solicitante uma Guia de Recolhimento da União (GRU) ou documento equivalente para que ele possa realizar o pagamento.</p>
			</div>

			<div class="title">
				<i class="dropdown icon"></i>
				8. Quais são os prazos para resposta dos pedidos apresentados com base na da Lei de Acesso à Informação?
			</div>
			<div class="content">
				<p>Se a informação estiver disponível, ela deve ser entregue imediatamente ao solicitante. Caso não seja possível conceder o acesso imediato, o órgão ou entidade tem até 20 (vinte) dias para atender ao pedido, prazo que pode ser prorrogado por mais 10 (dez) dias, se houver justificativa expressa.</p>
			</div>

			<div class="title">
				<i class="dropdown icon"></i>
				9. O que é transparência ativa?
			</div>
			<div class="content">
				<p>É a divulgação de dados por iniciativa do próprio setor público, ou seja, quando são tornadas públicas informações, independente de requerimento, utilizando principalmente a Internet. </p>
				<p>Um exemplo de transparência ativa são as seções de acesso à informações dos sites dos órgãos e entidades. Os portais de transparência também são um exemplo disso. </p>
				<p>A divulgação proativa de informações de interesse público, além de facilitar o acesso das pessoas e de reduzir o custo com a prestação de informações, evita o acúmulo de pedidos de acesso sobre temas semelhantes.</p>
			</div>

			<div class="title">
				<i class="dropdown icon"></i>
				10. O que é transparência passiva?
			</div>
			<div class="content">
				<p>É a disponibilização de informações públicas em atendimento a demandas específicas de uma pessoa física ou jurídica. Por exemplo, a resposta a pedidos de informação registrados para determinado Ministério</p>
			</div>

			<div class="title">
				<i class="dropdown icon"></i>
				11. Onde posso encontrar a Lei nº 12.537 na íntegra?
			</div>
			<div class="content">
				<p>A Lei nº 12.527 pode ser encontrada na íntegra, autêntica e atualizada <a target="_blank" href="http://www.planalto.gov.br/ccivil_03/_ato2011-2014/2011/lei/l12527.htm">clicando aqui</a></p>
			</div>
		</div>
	</div>
</div>
<script>
	$('.ui.accordion').accordion();
</script>
<?php include_once(__DIR__.'/../include/footer.php'); ?>