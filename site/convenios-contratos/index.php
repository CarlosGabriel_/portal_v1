<?php include_once(__DIR__.'/../include/header.php'); ?>

<div class="ui breadcrumb">
	<a href="../index" class="section">Início</a>
	<i class="right angle icon divider"></i>
	Contratos e Convênios
</div>

<div class="ui segments">
	<div class="ui segment">
		<h1>Contratos e Convênios</h1>
	</div>
 	<div class="ui secondary segment">
		<div class="ui divided list">
			<div class="item">
				<div class="content">
					<h3 class="header">
						<i class="right triangle icon"></i>
						<a href="contratos">Contratos</a>
					</h3>
				</div>
			</div>
			<div class="item">
				<div class="content">
					<h3 class="header">
						<i class="right triangle icon"></i>
						<a href="convenios">Convênios</a>
					</h3>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="voltar">
	<a href="../index"><< Voltar</a>
</div>
<?php include_once(__DIR__.'/../include/footer.php'); ?>