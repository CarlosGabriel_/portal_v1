<?php include_once(__DIR__.'/../include/header.php'); ?>

<div class="ui breadcrumb">
	<a href="../index" class="section">Início</a>
	<i class="right angle icon divider"></i>
	<a href="../legislacao" class="section">Legislação</a>
	<i class="right angle icon divider"></i>
	Leis Municipais
</div>

<div class="ui segments">
	<div class="ui segment">
		<h1>Leis Municipais</h1>
	</div>
 	<div class="ui secondary segment">
		<div class="ui big middle aligned very relaxed divided selection list">
			<a target="_blank" href="../arquivo/lom/LOM.pdf" class="item">
				<div class="content">
					<div class="header">Lei Orgânica Municipal nº 1.121/2009</div>
					<div class="description">
						Dispõe sobre a Organização Administrativa do município de Gurupá-Pa e dá outras providências.
					</div>
				</div>
			</a>
			<a target="_blank" href="../arquivo/leis/lei-municipal/Lei1121-2009.pdf" class="item">
				<div class="content">
					<div class="header">Lei 1.121 de 2009 </div>
					<div class="description">
						Organização Administrativa.
					</div>
				</div>
			</a>
			<a target="_blank" href="../arquivo/leis/lei-municipal/Lei774-1993.pdf" class="item">
				<div class="content">
					<div class="header">Lei 774 de 1993</div>
					<div class="description">
						Dispõe sobre a Alteração do Código Tributario Municipal.
					</div>
				</div>
			</a>
		</div>
	</div>
</div>

<div class="voltar">
	<a href="../index"><< Voltar</a>
</div>

<?php include_once(__DIR__.'/../include/footer.php'); ?>