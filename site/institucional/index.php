<?php include_once(__DIR__.'/../include/header.php'); ?>

<div class="ui breadcrumb">
	<a href="../index/" class="section">Início</a>
	<i class="right angle icon divider"></i>
	Institucional
</div>

<div class="ui segments">
	<div class="ui segment">
		<h1>Institucional</h1>
	</div>
 	<div class="ui secondary segment">
		<div class="ui divided list">
			<div class="item">
				<div class="content">
					<h3 class="header">
						<i class="right triangle icon"></i>
						<a href="departamentos">Secretarias e Departamentos</a>
					</h3>
				</div>
			</div>
			<div class="item">
				<div class="content">
					<h3 class="header">
						<i class="right triangle icon"></i>
						<a href="agenda-do-prefeito">Agenda do Prefeito</a>
					</h3>
				</div>
			</div>
			<div class="item">
				<div class="content">
					<h3 class="header">
						<i class="right triangle icon"></i>
						<a href="#">Contato</a>
					</h3>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="voltar">
	<a href="../index/"><< Voltar</a>
</div>
<?php include_once(__DIR__.'/../include/footer.php'); ?>